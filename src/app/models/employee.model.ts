export interface EmployeeModel {
  id: number;
  nip: string;
  name: string;
  nickName: string;
  birth: string;
  address: string;
  phone: string;
  mobile: string;
  email: string;
}
