import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { UserModel } from '../models/user.model';

export const SET_USER = 'SET_USER';
export const REMOVE_USER = 'REMOVE_USER';

export class SetUser implements Action {
  readonly type = SET_USER;

  constructor(public payload: UserModel) {}
}

export class RemoveUser implements Action {
  readonly type = REMOVE_USER;
}

export type Actions = SetUser | RemoveUser;
