import { EmployeeModel } from './models/employee.model';
import { UserModel } from './models/user.model';

export interface AppState {
  readonly user: UserModel;
  readonly employee: EmployeeModel[];
}
