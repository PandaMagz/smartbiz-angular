import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserModel } from './../models/user.model';
import { AppState } from './../app.state';
import { Observable } from 'rxjs';
import { EmployeeService } from '../employee.service';
import { EmployeeModel } from '../models/employee.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  user$: Observable<UserModel>;
  user: UserModel;

  search = '';
  pagination = 1;
  loading = true;

  dataSource = new MatTableDataSource<EmployeeModel>();
  displayedColumns: string[] = [
    'id',
    'nip',
    'name',
    'address',
    'mobile',
    'email',
    'action',
  ];

  constructor(
    private store: Store<AppState>,
    private employeeService: EmployeeService,
    private auth: AuthService,
    private route: Router
  ) {
    this.user$ = store.select('user');
    this.user$.subscribe((data) => {
      this.user = data;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  editTrainer(id: number, edit: any) {
    this.route.navigate(['/employee', id, edit.toString()]);
    console.log(id);
  }

  getEmployee(pagination: boolean) {
    this.loading = true;
    if (!pagination) {
      this.dataSource.data = [];
      this.pagination = 1;
    }
    this.employeeService
      .searchEmployee({
        search: this.search.length > 0 ? this.search : '',
        birthDate: '',
        pagination: this.pagination,
      })
      .subscribe(
        (res) => {
          if (this.pagination < res.meta.pages) {
            this.pagination = res.meta.pages;
            this.getEmployee(true);
          }

          res.rows.map((data) => {
            this.dataSource.data = [
              ...this.dataSource.data,
              {
                id: data.id,
                nip: data.nip,
                name: data.full_name,
                nickName: data.nick_name,
                birth: data.birth_date.toString(),
                address: data.address,
                phone: data.phone,
                mobile: data.mobile,
                email: data.email,
              },
            ];
          });
          this.loading = false;
        },
        () => {
          this.route.navigate(['/login']);
          localStorage.clear();
        }
      );
  }

  ngOnInit(): void {
    this.getEmployee(false);
    // this.auth
    //   .refreshToken(localStorage.getItem('refreshToken'))
    //   .subscribe((data) => {
    //     console.log(data.data.token.access_token);
    //   });
  }
}
