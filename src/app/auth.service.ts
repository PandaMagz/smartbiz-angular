import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ILoginResponse, IRefreshTokenResponse } from 'src/types/responseApi';

export interface IUser {
  identity: string;
  password: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private URL = 'https://api.smartbiz.id/api/login';
  private refreshTokenURL = 'https://api.smartbiz.id/api/token-refresh';
  constructor(private http: HttpClient) {}

  loginUser(user: IUser) {
    return this.http.post<ILoginResponse>(this.URL, user);
  }

  refreshToken(refreshToken: string) {
    return this.http.post<IRefreshTokenResponse>(this.refreshTokenURL, {
      refresh_token: refreshToken,
    });
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }
}
