import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IEmployeeResponse, IGetEmployeeResponse } from 'src/types/responseApi';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private searchURL = 'https://api.smartbiz.id/api/demo/employee/page-search';
  private getEmployeeURL = 'https://api.smartbiz.id/api/demo/employee';
  constructor(private http: HttpClient) {}

  getEmployee(id: string): Observable<IEmployeeResponse> {
    return this.http.get<IEmployeeResponse>(`${this.getEmployeeURL}/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  }

  removeEmployee(id: string): Observable<IEmployeeResponse> {
    return this.http.delete<IEmployeeResponse>(`${this.getEmployeeURL}/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  }

  updateEmployee(
    id: string,
    data: IEmployeeResponse
  ): Observable<IEmployeeResponse> {
    return this.http.put<IEmployeeResponse>(
      `${this.getEmployeeURL}/${id}`,
      {
        id: data.id,
        nip: data.nip,
        full_name: data.full_name,
        nick_name: data.nick_name,
        birth_date: data.birth_date,
        address: data.address,
        phone: data.phone,
        email: data.email,
        mobile: data.mobile,
        created_by: 'demo',
        modified_by: null,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    );
  }

  createEmployee(data: IEmployeeResponse): Observable<IEmployeeResponse> {
    return this.http.post<IEmployeeResponse>(
      this.getEmployeeURL,
      {
        id: data.id,
        nip: data.nip,
        full_name: data.full_name,
        nick_name: data.nick_name,
        birth_date: data.birth_date,
        address: data.address,
        phone: data.phone,
        email: data.email,
        mobile: data.mobile,
        created_by: 'demo',
        modified_by: null,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    );
  }

  searchEmployee(filter: {
    search: string;
    birthDate: string;
    pagination: number;
  }): Observable<IGetEmployeeResponse> {
    return this.http.post<IGetEmployeeResponse>(
      this.searchURL,
      {
        query: {
          value: filter.search,
        },
        start_birth_date: filter.birthDate,
        end_birth_date: '',
        pagination: {
          page: filter.pagination,
          perpage: 10,
        },
        sort: {
          sort: 'ASC',
          field: 'id',
        },
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    );
  }
}
