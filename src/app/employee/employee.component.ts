import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IEmployeeResponse } from 'src/types/responseApi';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit {
  private sub: any;
  user: IEmployeeResponse = {
    id: 1,
    nip: '',
    full_name: '',
    nick_name: '',
    birth_date: null,
    address: '',
    phone: '',
    email: '',
    mobile: '',
    created_by: '',
    modified_by: '',
  };

  errorMessage = {
    address: '',
    email: '',
    full_name: '',
    nip: '',
  };

  id: string;
  edit = false;

  loading = false;

  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private auth: AuthService,
    private router: Router
  ) {}

  onSubmit() {
    this.loading = true;
    if (this.edit) {
      this.employeeService.updateEmployee(this.id, this.user).subscribe(
        (res) => {
          this.router.navigate(['/dashboard']);
          this.loading = false;
        },
        (error) => {
          this.loading = false;
          if (error.error.status === 401) {
            this.router.navigate(['/login']);
            return;
          }
          console.log(error.error);
          this.errorMessage.address =
            error.error.message.address !== undefined
              ? error.error.message.address[0]
              : '';
          this.errorMessage.email =
            error.error.message.email !== undefined
              ? error.error.message.email[0]
              : '';
          this.errorMessage.full_name =
            error.error.message.full_name !== undefined
              ? error.error.message.full_name[0]
              : '';
          this.errorMessage.nip =
            error.error.message.nip !== undefined
              ? error.error.message.nip[0]
              : '';
        }
      );
      return;
    }
    this.employeeService.createEmployee(this.user).subscribe(
      () => {
        this.loading = false;
        this.router.navigate(['/dashboard']);
      },
      (error) => {
        this.loading = false;
        if (error.error.status === 401) {
          this.router.navigate(['/login']);
          return;
        }
        console.log(error.error);
        this.errorMessage.address =
          error.error.message.address !== undefined
            ? error.error.message.address[0]
            : '';
        this.errorMessage.email =
          error.error.message.email !== undefined
            ? error.error.message.email[0]
            : '';
        this.errorMessage.full_name =
          error.error.message.full_name !== undefined
            ? error.error.message.full_name[0]
            : '';
        this.errorMessage.nip =
          error.error.message.nip !== undefined
            ? error.error.message.nip[0]
            : '';
      }
    );
  }

  onDelete() {
    this.loading = true;
    this.employeeService.removeEmployee(this.id).subscribe((res) => {
      this.router.navigate(['/dashboard']);
    });
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      if (params['edit'] === 'true') {
        this.loading = true;
        this.edit = true;
        this.id = params['id'];
        this.employeeService.getEmployee(this.id).subscribe((data) => {
          this.user = data;
          this.user.birth_date = new Date(this.user.birth_date);
          this.loading = false;
        });
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
