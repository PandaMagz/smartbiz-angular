import { Component, OnInit } from '@angular/core';
import { AuthService, IUser } from '../auth.service';
import { Store } from '@ngrx/store';
import { AppState } from './../app.state';
import * as UserActions from './../actions/user.action';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginUserData: IUser = { identity: '', password: '' };
  loading = false;
  errorMessage = '';

  constructor(
    private auth: AuthService,
    private store: Store<AppState>,
    private route: Router
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    this.loading = true;
    this.auth.loginUser(this.loginUserData).subscribe(
      (res) => {
        this.loading = false;
        localStorage.setItem('token', res.data.token.access_token);
        localStorage.setItem('refreshToken', res.data.token.refresh_token);
        this.store.dispatch(
          new UserActions.SetUser({
            username: res.data.user.username,
            email: res.data.user.email,
          })
        );
        this.route.navigate(['/dashboard']);
      },
      (err) => {
        this.loading = false;
        this.errorMessage = err.error.message;
        console.log(err.error.message);
      }
    );
  }
}
