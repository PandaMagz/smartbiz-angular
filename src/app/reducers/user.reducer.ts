import { Action } from '@ngrx/store';
import { UserModel } from '../models/user.model';
import * as UserActions from './../actions/user.action';

const initialState: UserModel = {
  username: '',
  email: '',
};

export function reducer(state = initialState, action: UserActions.Actions) {
  switch (action.type) {
    case UserActions.SET_USER:
      return {
        ...state,
        username: action.payload.username,
        email: action.payload.email,
      };
    case UserActions.REMOVE_USER:
      return initialState;
    default:
      return state;
  }
}
