export interface IBaseResponse {
  status: number;
  message: string;
}

export interface IRefreshTokenResponse extends IBaseResponse {
  data: {
    token: {
      token_type: string;
      expires_in: number;
      access_token: string;
      refresh_token: string;
    };
  };
}

export interface ILoginResponse extends IBaseResponse {
  data: {
    user: {
      id: number;
      username: string;
      email: string;
      applications: any[];
      groups: {
        id: number;
        name: string;
        slug: string;
      }[];
      roles: {
        id: number;
        name: string;
        slug: string;
        permissions: any[];
      }[];
      companies: any[];
    };
    token: {
      token_type: string;
      expires_in: number;
      access_token: string;
      refresh_token: string;
    };
  };
}

export interface IEmployeeResponse {
  id: number;
  nip: string;
  full_name: string;
  nick_name: string;
  birth_date: Date | null;
  address: string;
  phone: string;
  email: null | string;
  mobile: string;
  created_by: string;
  modified_by: null | string;
}

export interface IGetEmployeeResponse {
  meta: {
    page: number;
    pages: number;
    perpage: number;
    total: number;
    sort: 'ASC' | 'DESC';
    field: 'id';
  };
  rows: IEmployeeResponse[];
}
